#!/usr/bin/python

# Modules

import os
import subprocess as sp
import webbrowser
import wikipedia
from weather import Weather
import sys
from pytvdbapi import api
import re
from random import randint
import linecache
import gmail
import datetime
from simplecrypt import encrypt, decrypt
import getpass
import time
from cryptography.fernet import Fernet
from Crypto.Cipher import AES
import webbrowser

os.system("clear") # Clearing Screen

# Functions
# File opener
def fileopen(filename):
    Information = open(filename, "r")
    Data = Information.readline()
    Information.close()
    return Data

# Date
def Date():
    now = datetime.datetime.now()
    return now

# Length function
def filelength(filename):
    numberofline = sum(1 for line in open(filename))
    return numberofline

# Random number
def Random(start,end):
    answer = randint(start, end)
    return answer

# Line reader
def LineReader(filename,linenumber):
    line = linecache.getline(filename, linenumber)
    return line

# random line
def randomline(filename):
    lengthoffile = filelength(filename)
    randomlinenumber = Random(1,lengthoffile)
    answer = LineReader(filename,randomlinenumber)
    return answer

# Conversation Engine
def conversation(filename):
    answerneeded = randomline("Conversations/"+filename)
    print answerneeded

# Ask
def ask(filename):
    def newconversation(filename):
        answerneeded = randomline("Conversations/"+filename)
        output = raw_input(answerneeded)
        return output

# write
def Writer(filename,Variable):
        Data = open(filename, "w")
        Data.write(Variable)
        Data.close()

# Clearing
def Clear():
    os.system("clear")

# Name Engine
def NameEngine():
    Name = fileopen("UserData/name.txt")
    FullName = Name
    Name = Name.split()
    FirstName = Name[0]
    LastName = Name[1]
    Name = Name[0]

    return FirstName,LastName,FullName,Name

# Not configured
def Config():
    newusr=raw_input("Hey there! I am kitt and I wanted to know who you're: ")
    Writer("UserData/name.txt",newusr)
    FirstName,LastName,FullName,Name = NameEngine() # Name Engine
    print "Hello, "+Name+"! It's good to see you!"
    print ""
    print "Do you what to setup your email client?"
    newusr=raw_input("NOTE: ONLY GMAIL IS CURRENTLY SUPPORTED! ")
    newusr = newusr.lower()
    if newusr == "y" or newusr == "yes":
        Clear()
        print "NOTE: Your Username and Password will be encrypted and stored in your computer."
        print "I don't have any access to your E-Mail information, my MailEngine will only use it"
        print ""
        UserName=raw_input("Your Gmail Username: ")
        PassWord=getpass.getpass("Your Gmail Password: ")
        time.sleep(3)
        print "Sending Email information to my EmailEngine"
        EmailInformation(UserName,PassWord)
        time.sleep(1)
        print "Deleting your Email information from my brain"
        UserName = "Removed"
        PassWord = "Removed"
        Clear()
        print "Congratulations, "+Name+"! I setted up your Email Client."
        print "To set your weather details please enter your city for getting proper updates"
        City = raw_input("Your city please: ")
        Writer("UserData/city.txt",City)
        print "Thanks,"+Name+" for setting these things up!"
    else:
        print "To set your weather details please enter your city for getting proper updates"
        City = raw_input("Your city please: ")
        Writer("UserData/city.txt",City)
        print "Thanks,"+Name+" for setting these things up!"

def Username():
    Cipher = fileopen("EMAIL/Username.txt")
    Username = decrypt("123123", Cipher)
    return Username

def Password():
    Cipher = fileopen("EMAIL/Password.txt")
    Password = decrypt("123123", Cipher)
    return Password

def Encryption(filename,Variable):
    Cipher = encrypt("123123", Variable)
    Writer(filename,Cipher)

def Decryption(filename):
    Cipher = fileopen(filename)
    Password = decrypt("123123", Cipher)
    return Cipher

def UsernameSaver(Variable):
    Cipher = encrypt("123123", Variable)
    Writer("EMAIL/username.txt",Cipher)

def PasswordSaver(Variable):
    Cipher = encrypt("123123", Variable)
    Writer("EMAIL/password.txt",Cipher)

def EmailInformation(Username,Password):
    UsernameSaver(Username)
    PasswordSaver(Password)

# Mail Engine
def MailEngine():
    UserName = Username()
    PassWord = Password()
    g = gmail.login(UserName, PassWord)
    g.inbox().mail()
    unread = g.inbox().mail(unread=True, after=datetime.date(now.year, now.month, now.day) )
    unread[1].fetch()
    Data = len(unread)
    return Data

def BootScreen():
        print "Booting."
        time.sleep(1)
        Clear()
        print "Booting.."
        time.sleep(1)
        Clear()
        print "Booting..."
        time.sleep(1)
        Clear()
        print "Booting...."
        time.sleep(3)

def LowerCase(Variable):
    Variable = Variable.lower()
    return Variable


"<----------------------------------------------------------------------------->"
FirstName,LastName,FullName,Name = NameEngine() # Name Engine
name = Name
if "GetStarted" in FullName:
    Config() # Configuring kitt
else:
    BootScreen()
    Clear()
#    City = fileopen("UserData/city.txt")
    os.system("clear") # Clearing Screen
 #   weather = Weather() # Weather Variable
    WelcomeCount = 0 # Welcome Variable
  #  location = weather.lookup_by_location('dublin') # Location
    #location = weather.lookup_by_location(City) # Location Variable
   # condition = location.condition() # Weather Manupilation
    #weather = condition.text() # Answer

    """
    location = weather.lookup_by_location(City) # Location Variable
    condition = location.condition() # Weather Manupilation
    AnswerWeather = condition.text()
    """
    donothing = "Do nothing!" # Empty Variable
    invalidnumber = 0 # invalidnumber
    while True: # Making a infinity loop
        if WelcomeCount != 0:
            usrinput = "RETURN"
            usrinput = raw_input(name + ", what do you want me to do? ")
            re.sub(r'[^\w]', ' ', usrinput)
        elif WelcomeCount == 0:
            condition = location.condition()
            weather = condition[City]

            print "Welcome, " + name +"! It seems to be " + weather + " outside."
            usrinput = raw_input("So, what are you doing? If I can ask? ")
            if usrinput=="Yes" or usrinput=="yes":
                Information = raw_input("So, go ahead " + name + ": ")
                ask = raw_input("So, I see that you're doing something. Do you want me to upload your doings to my brain(My database)? ")
                WelcomeCount = WelcomeCount + 1
                if ask=="Yes" or ask=="yes":
                    f = os.popen('date' + "\n")
                    now = f.read()
                    hs = open("HumanLOGS.txt","a")
                    hs.write(now)
                    hs.write(Information)
                    hs.write("\n<-------------------------------------------------->\n")
                    hs.close()
                    print "I've saved it to my brain."
                    usrinput = "RETURN"
                    WelcomeCount = WelcomeCount + 1
                    continue
                else:
                    print "Okay! I guess your doing something which is private so I wont save it."
                    usrinput = "RETURN"
                    usrinput = raw_input("So, " + name + ". Want any help? ")
                    WelcomeCount = WelcomeCount + 1
            else:
                print "Okay! I guess your doing something which is private so I wont save it."
                usrinput = "RETURN"
                usrinput = raw_input("So, " + name + ". Want any help? ")
                WelcomeCount = WelcomeCount + 1
        usrinput=usrinput.lower() # Coverting Userinput LowerCase
        # Calculations for precise answering.
        WhatIs = usrinput[:7]
        WhoIs = usrinput [:6]

        length = len(usrinput)
        precisevar = length - 1
        precise = usrinput[precisevar:]
        if precise == "!" or precise == "?":
            usrinput = usrinput[:precisevar]
        else :
            donothing = "Whatever"

        if usrinput == "exit" or usrinput == "bye" or usrinput == "good bye":
            print "Good bye "+name+"! Had a good time with you."
            break
        elif usrinput == "what's the date" or usrinput == "what is the date" or usrinput == "date":
            f = os.popen('date')
            now = f.read()
            print "Today is ", now
        elif usrinput == "what's the time" or usrinput == "what is the time" or usrinput == "time":
            f = os.popen('date')
            now = f.read()
            print "Today is ", now
        elif usrinput=="add project" or usrinput=="make a new project":
            usrinput=raw_input(name+", should I put this project on Github server? ")
            if usrinput=="Yes" or usrinput=="yes":
                print "But "+name+" I don't have the permissions for accesing Github server right now."
                usrinput = raw_input("Do you want me to store it in our local system environment ")
                usrinput=usrinput.lower() # Coverting Userinput LowerCase
                length = len(usrinput)
                precisevar = length - 1
                precise = usrinput[precisevar:]
                if precise == "!" or precise == "?":
                    usrinput = usrinput[:precisevar]
                else :
                    donothing = "Whatever"
                gitdate = os.popen('date')
                donothing = "Whatever"
                if usrinput == "yes" or usrinput == "y":
                    os.system("mkdir NewProject_"+gitdate)
                    projectname = "\"NewProject_"+gitdate+"\""
                    print "I've created a project called "+projectname+". All the best on your new project "+name+"!"
                    continue
            else:
                now = datetime.datetime.now()
                gitdate = now.strftime("%Y-%m-%d %H:%M")
                gitdate = str(gitdate)
                os.system("mkdir NewProject_"+gitdate+" ")
                projectname = "\"NewProject_"+gitdate+"\""
                print "I've created a project called "+projectname+". All the best on your new project "+name+"!"
                continue
        elif WhatIs == "What is" or WhatIs == "what is":
            Query = usrinput
            Length = len(Query)
            NewQueryLength = Length - 8
            NewQueryLength = NewQueryLength*-1
            Query = Query[NewQueryLength:]
            #print Query
            print wikipedia.summary(Query, sentences=1)
        elif WhoIs == "Who is" or WhoIs == "who is":
            WhoQuery = usrinput
            WhoLength = len(WhoQuery)
            WhoNewQueryLength = WhoLength - 6
            WhoNewQueryLength = WhoNewQueryLength*-1
            WhoQuery = WhoQuery[WhoNewQueryLength:]
            #print WhoQuery
            print wikipedia.summary(WhoQuery, sentences=1)
        elif usrinput == "restart kitt":
            continue
        elif usrinput == "clear":
            os.system("clear")
            continue
        elif usrinput == "kitt":
            conversation("kitt.txt")
        elif usrinput == "test my internet" or usrinput == "is internet up" or usrinput == "internet is there":
            os.system("speedtest-cli")
        elif usrinput == "check my internet" or usrinput == "is my internet up" or usrinput == "internet is there":
            os.system("speedtest-cli")
        elif usrinput == "tell me about my computer" or usrinput == "my computer" :
            os.system('screenfetch')
        elif usrinput == "fetch me about my computer" or usrinput == "check computer" :
            os.system('screenfetch')
        elif usrinput=="force quit" :
            os.system('telnet nyancat.dakko.us')
        elif usrinput=="open Google" :
            webbrowser.open('http://google.co.in', new=2)
        else:
            conversation("InvalidQuestion.txt")
            YesNo = raw_input("Do you want me to Google it? ")
            YesNo = LowerCase(YesNo)
            if YesNo == "yes" or YesNo == "y":
                usrinput = "https://www.google.com/#q="+usrinput+""
                webbrowser.open(usrinput, new=2)
  

